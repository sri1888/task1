import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Customer } from '../Services/customer';
import { ActivatedRoute } from '@angular/router';
import { CustomersService } from '../Services/customers.service';
import { MatDialog } from '@angular/material';
import { EditCustomerComponent } from '../edit-customer/edit-customer.component';

@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.css']
})
export class CustomerDetailsComponent implements OnInit {
  id: any;
  customer: Customer;
  isSaved: boolean;
  constructor(private router: ActivatedRoute, private customerservice: CustomersService, public dialog: MatDialog,private detect: ChangeDetectorRef)  { }

  ngOnInit() {
    this.id = this.router.snapshot.paramMap.get("id");

    this.loadCustomerData();
  }

  loadCustomerData(){
    console.log(this.id);
    this.customerservice.getContactByID(this.id)
      .subscribe((res: Customer) => {
        console.log(res);
        this.customer = res;
      });
  }

  
  openEditDialog() {
    const dialogRef = this.dialog.open(EditCustomerComponent,
      {
        width: '400px',
        data: JSON.parse(JSON.stringify(this.customer))
      });

    dialogRef.afterClosed().subscribe(async (updatedContactData: any) => {
      // alert('check');
      console.log(updatedContactData);

      if (updatedContactData != 'nope') {
        const status: any = await this.customerservice.updateContact(updatedContactData)
        if (status && status.customerID) {
          this.loadCustomerData();
          console.log('status');
          this.detect.detectChanges();
          this.isSaved = true;
        } else {
          console.log('status');
        }

        console.log('The dialog was closed');

      } else {

      }

    });
  }
}
