import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Customer } from '../Services/customer';
import { CustomersService } from '../Services/customers.service';

@Component({
  selector: 'app-customers-list',
  templateUrl: './customers-list.component.html',
  styleUrls: ['./customers-list.component.css']
})
export class CustomersListComponent implements OnInit {
  contactList: Customer[];
  contactsSubscription: Subscription;
  constructor(private customerservice: CustomersService) { }

  ngOnInit() {
    this.contactsSubscription = this.customerservice.getContacts()
      .subscribe((res: Customer[]) => {
        console.log(res);
        this.contactList = res;

      });
  }

  ngOnDestroy() {
    console.log('Into Destroy Lifecycle hook');
    // ideal place for us to unscubscribe and empty the data
    this.contactsSubscription.unsubscribe();

    if (this.contactList && this.contactList.length > 0) {
      this.contactList.length = 0;
    }
  }

}
