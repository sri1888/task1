import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Customer } from './customer';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {
  REST_API_URL = 'http://localhost:3000/api/contacts';
  constructor(private http: HttpClient) { }

  createContact( contactData: any ){
    console.log(contactData);
   

    const createContactPromise = new Promise( (resolve, reject) =>{
      this.http.post(this.REST_API_URL, contactData)
            .toPromise()
            .then( ( res: Customer) => {
              console.log(res);
              resolve(res);
            })
            .catch( (err: any) => {
              console.log(err);
              reject(err);
            })
            .finally( () =>{
              console.log('Its Over');
            });
    } );

    return createContactPromise as Promise<Customer>;

    
  }

  getContacts(): Observable<Customer[]>{
    return this.http.get(this.REST_API_URL)
    .pipe( map((res: Customer[]) =>{
        console.log(res);      
        return res;
      }));
  }

  getContactByID(customerID:number): Observable<Customer>{
    console.log(customerID);
    return this.http.get(this.REST_API_URL+"/"+customerID)
    .pipe( map((res: Customer) =>{
      console.log(res);      
      return res;
    }));
  }

  updateContact(upddateableContactData){
    console.log(upddateableContactData);

    const updateContactPromise = new Promise( (resolve, reject) =>{  
      this.http.put(this.REST_API_URL+'/'+upddateableContactData.customerID, upddateableContactData)
        .toPromise()
        .then((res: any) => {
          console.log(res);
          resolve(res);
        })
        .catch((err: any) => {
          console.log(err);
          reject(err);
        })
        .finally(() => {
          console.log('Its Over');
        });
    });
    return updateContactPromise as Promise<any>;

  }

  
}
