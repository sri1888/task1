import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CustomersService } from '../Services/customers.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addcustomer',
  templateUrl: './addcustomer.component.html',
  styleUrls: ['./addcustomer.component.css']
})
export class AddcustomerComponent implements OnInit {
  isSaved: boolean;
  isPresent:boolean;
  // Step1:create formGroup
  contactForm: FormGroup;
  Genders: string[] = ['Male','Female']

  constructor(private customerservice: CustomersService, private router: Router) { }

  ngOnInit() {
    this.contactForm=new FormGroup({
      // Step 3: Create FormControls
      customerID: new FormControl('',Validators.required),
      name: new FormControl('',Validators.required),  //Step6 Add Validators
      email: new FormControl('',[Validators.required,
                                 Validators.email]),
      phone: new FormControl('',Validators.required),
      gender: new FormControl('')
    });
  }


 async onCreateCustomer() {
   console.log(this.contactForm.value)
    const status: any = await this.customerservice.createContact(this.contactForm.value);// 3. get the respone from the service 
    console.log(status);
    if(status.code == 200){//  4. patch the resp with html
      this.isSaved = true;
      this.router.navigate(['']);
    } else if(status.code == 201){
      this.isPresent=true;
    }

  }
}
