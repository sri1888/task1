import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddcustomerComponent } from './addcustomer/addcustomer.component';
import { CustomersListComponent } from './customers-list/customers-list.component';
import { EditCustomerComponent } from './edit-customer/edit-customer.component';
import { CustomerDetailsComponent } from './customer-details/customer-details.component';


const routes: Routes = [{
  path:'addcustomer',component:AddcustomerComponent
},
{
path:'',component:CustomersListComponent
},{
  path:'edit-customer',component:EditCustomerComponent
},{
  path:'customer/:id',component:CustomerDetailsComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
