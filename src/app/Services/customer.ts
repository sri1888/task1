interface ICustomer{
    id:number;
    name: string;
    email: string;
    phone: string;
    gender: string
}
export class Customer implements ICustomer{
    id: number;
    name: string;
    email: string;
    phone: string;
    gender: string
}